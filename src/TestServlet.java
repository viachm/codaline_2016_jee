import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

//@WebServlet("/test")
public class TestServlet extends HttpServlet {

    private static final String SESSION_INDEX_KEY = "index";

    protected void doPost(
            HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {
        System.out.println("POST reqeust!");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer sessionIndex = (Integer) session.getAttribute(SESSION_INDEX_KEY);
        sessionIndex = sessionIndex == null ? 1 : sessionIndex + 1;
        session.setAttribute(SESSION_INDEX_KEY, sessionIndex);

        PrintWriter out = response.getWriter();
        out.print("<h1>Session index = " + sessionIndex + "</h2>");
    }
}
